<?php
require 'vendor/autoload.php';

use WpEngineExercise\Csv;
use WpEngineExercise\AccountsProcessor;
use WpEngineExercise\RemoteApi;

function RunProgram()
{
    // Create CSV, RemoteApi and AccountProcessor instances
    $csv = new Csv();
    $remoteApi = new RemoteApi();
    $accountsProcessor = new AccountsProcessor($remoteApi);

    // Read the data from the local filesystem
    echo "Reading CSV file\n";
    $inputCsv = $csv->readData();
    // Procescs the local data, adding status fields
    echo "Processing data\n";
    $processedCsv = $accountsProcessor->process($inputCsv);
    // Write the processed Data to file
    echo "Writing output file\n";
    $csv->writeData($processedCsv);
    echo "Hit 'q' to quit or any other key to run again\n";
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    if(trim($line) == 'q'){
        echo "ABORTING!\n";
        exit;
    }else{
        RunProgram();
    }
    fclose($handle);
}

RunProgram();

