<?php
/**
 * Created by PhpStorm.
 * User: Beedge
 * Date: 28/07/2016
 * Time: 16:33
 */

namespace WpEngineExercise;


interface iRequest
{
    public function CallAPI($method, $url, $data = false);
}