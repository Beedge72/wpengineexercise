# README #

WP Engine Coding Exercise for Kevin Bradshaw

### Installation ###

Download the Repository. You will need to have have Composer installed. 

From the command line navigate to the root of the project and type: *composer install*

Once dependencies are loaded to run program type: *php input.php*

The program will run through and write a file  (output.csv) to the data folder

 