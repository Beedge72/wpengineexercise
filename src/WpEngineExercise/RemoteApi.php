<?php
/**
 * Created by PhpStorm.
 * User: Beedge
 * Date: 26/07/2016
 * Time: 11:19
 */


namespace WpEngineExercise;


class RemoteApi implements iRequest
{
    /**
     * Curl call to Remote server to retrieve missing status data on each account
     *
     * @param $method
     * @param $url
     * @param bool $data
     * @return mixed
     */
    public function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

}