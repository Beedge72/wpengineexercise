<?php
use \WpEngineExercise\AccountsProcessor;

class AccountsProcessorTest extends PHPUnit_Framework_TestCase
{
    protected $processor;

    /**
     * Create a mock object to represent the RemoteApi object that
     * returns correctly formatted data
     */
    protected function setUp()
    {
        // Set a map of responses based on calls to API
        $map = [
            ["GET", "http://interview.wpengine.io/v1/accounts/12345",false, '{"account_id":12345,"status":"good","created_on":"2011-01-12"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/8172",false, '{"account_id":8172,"status":"closed","created_on":"2015-09-01"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/1924",false, '{"account_id":1924,"status":"fraud","created_on":"2012-03-01"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/8172",false, '{"account_id":222222,"status":"poor","created_on":"2014-01-02"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/222222",false, '{"account_id":48213,"status":"high risk","created_on":"2015-08-15"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/918299",false, '{"account_id":918299,"status":"good","created_on":"2014-06-01"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/88888",false, '{"account_id":88888,"status":"collections","created_on":"2015-08-08"}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/8",false, '{"detail":"Not found."}'],
            ["GET", "http://interview.wpengine.io/v1/accounts/88",false, '{"detail":"Not found."}']
        ];
        // Create a stub method of API
        $stub = $this->getMock('\WpEngineExercise\iRequest');
        $stub->expects($this->any())
            ->method('CallAPI')
            ->will($this->returnValueMap($map));

        //Instanciate P AccountsProcessor with iRequest stub instead of RemoteApi instance
        $this->processor = new AccountsProcessor($stub);
    }

    /** @test */
    public function it_works_with_correctly_formatted_data()
    {
        // Input valid account data
        $inputCsv = [
            ["12345", "Beedge", "Kevin", "5/24/16"],
            ["8172", "Joe", "Bloggs", "1/1/12"]
        ];
        // Run the process
        $merged = $this->processor->process($inputCsv);
        // Assert we receive the expected information
        $this->assertEquals("12345", $merged[0][0]);
        $this->assertEquals("good", $merged[0][3]);
        $this->assertEquals("8172", $merged[1][0]);
        $this->assertEquals("closed", $merged[1][3]);
    }

    /** @test */

    public function it_handles_missing_status_data()
    {
        // Input valid account data
        $inputCsv = [
            ["8", "Beedge", "Kevin", "5/24/16"],
            ["88", "Joe", "Bloggs", "1/1/12"]
        ];
        // Run the process
        $merged = $this->processor->process($inputCsv);
        // Assert we receive the expected information
        $this->assertEquals("8", $merged[0][0]);
        $this->assertEquals(null, $merged[0][3]);
        $this->assertEquals("88", $merged[1][0]);
        $this->assertEquals(null, $merged[1][3]);
    }

    /** @test */

    public function it_handles_empty_data_sets()
    {
        // Leave input data as an empty array
        $inputCsv = [];
        // Result should be an empty array
        $merged = $this->processor->process($inputCsv);
        $this->assertCount(0, $merged);
    }

}