<?php
use \WpEngineExercise\Csv;
class CsvTest extends PHPUnit_Framework_TestCase
{
    protected $csv;

    protected function setUp()
    {
        $this->csv = new Csv();
    }

    /** @test */
    public function it_reads_the_file_header_correctly()
    {
        $header = $this->csv->readHeader();
        // Check the first element matches our expected header
        $this->assertEquals('Account ID,Account Name,First Name,Created On', $header);
    }

    /** @test */
    public function it_has_data()
    {
        $data = $this->csv->readData();
        // Check that there is data being read from file
        $this->assertNotCount(0, $data);
    }

    /** @test */
    public function it_writes_the_file()
    {
        //delete existing output if present
        // WARNING! one would never delete data on a production server for testing!!
        unlink("data/output.csv");
        $this->assertFileNotExists('data/output.csv');
        $merged_data = [
            ["8", "Beedge", "Kevin", "2014-5-5","poor","2014-5-5"],
            ["88", "Joe", "Bloggs", "2014-5-5","poor","2014-5-5"]
        ];

        $this->csv->writeData($merged_data);

        $this->assertFileExists('data/output.csv');
    }
}