<?php
/**
 * Created by PhpStorm.
 * User: Beedge
 * Date: 26/07/2016
 * Time: 11:19
 */

namespace WpEngineExercise;


class AccountsProcessor
{

    protected $remoteAccountData = [];
    protected $remoteApi;

    public function __construct(iRequest $remoteApi)
    {
        $this->remoteApi = $remoteApi;
    }

    /**
     * Main processor method.
     * Loads remote data, merging with supplied data
     * and returning resulting array
     *
     * @param $inputCsv
     * @return array
     */
    public function process($inputCsv): array
    {
        $this->loadRemoteData($inputCsv);
        return $this->mergeData($inputCsv);
    }

    /**
     * Takes the Data from the input file
     * iterates each account and adds missing data from remote server
     * adding it to a local array
     * Server Data is decoded from JSON string
     * Dates are formatted for consistancy
     *
     * Local array of combined data is returned
     *
     * @return array
     */
    protected function mergeData($inputCsv) :array
    {
        // Initialise local array to hold merge fields
        $local_data = [];
        foreach ($inputCsv as $account)
        {
            $status_data = json_decode($this->remoteAccountData[$account[0]]);
            // Check to see if status data was returned for this account
            // failed remote look ups return a 'detail' field, so checking for
            // the existance of detail indicates success of failure of lookup
            if(!isset($status_data->detail))
            {
                // Detail field not present,
                // set status and status_created_on fields
                $status = $status_data->status;
                $status_created_on = date_format(date_create($status_data->created_on), 'Y-m-d');
            }
            else{
                // Detail field present, set status values to null
                $status = null;
                $status_created_on = null;
            }
            $created_on = date_format(date_create($account[3]), 'Y-m-d');
            $local_data[] = [$account[0],$account[2],$created_on,$status,$status_created_on];
        }

        return $local_data;
    }

    /**
     * Given a data retrieved from Local CSV file, iterate each account, retrieve status info from server
     * and save result to instance variable array remoteAccountData
     *
     * @param $inputCsv
     */
    protected function loadRemoteData($inputCsv)
    {
        foreach ($inputCsv as $account)
        {
            // If a valid ID is found
            if(intval($account[0]) > 0) {
                // Lookup status data on remote server for this account and add it to RemoteAccountData Array
                $this->remoteAccountData["{$account[0]}"] =
                    $this->remoteApi->CallAPI("GET", "http://interview.wpengine.io/v1/accounts/{$account[0]}");
            }
        }
    }
}