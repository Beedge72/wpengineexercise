<?php namespace WpEngineExercise;


class Csv
{
    // String to contain raw data read from file
    private $csvFileString;

    function __construct()
    {
        // Read the data from file
        $this->csvFileString = file('data/input.csv')[0];
    }

    /**
     * Read the data in our CSV file
     *
     * @return array
     */
    public function readData() :array
    {
        $csvArray = $this->readFile();
        array_shift($csvArray);
        foreach($csvArray as $key => $row)
        {
            $csvArray[$key] = explode(",",$row);
        }
        return $csvArray;
    }

    /**
     * Public access to Create File method
     *
     * @param $mergedData
     */
    public function writeData(array $mergedData){
        $this->createFile($mergedData);
    }

    /**
     * Create the update CSV file
     *
     * @param $content
     */
    protected function createFile(array $content)
    {
        // create a file pointer connected to the output stream
        $output = fopen("data/output.csv", 'w');

        // output the column headings
        fputcsv($output, ["Account ID","First Name","Created On","Status","Status Set On"], ',', chr(0));
        // Iterate content and write to file
        foreach($content as $line)
        {
            fputcsv($output,$line, ',', chr(0));
        }

        // Close file pointer
        fclose($output);
    }

    /**
     * Return the Header from our input file
     *
     * @return string
     */
    public function readHeader() :string
    {
        $csvArray = $this->readFile();
        return $csvArray[0];
    }

    /**
     * Read the raw input file data and convert to an array based on line breaks
     *
     * @return array
     */
    protected function readFile() :array
    {
        // Convert the data string to an array by breaking on line breaks
        return  preg_split("/\\r\\n|\\r|\\n/", $this->csvFileString);
    }
}